import React from "react";
import { TiContacts } from "react-icons/ti";

const Contact = () => {
  return (
    <div className="central-container">
      <div className="circle">
        <div className="contact-icon">
          <TiContacts size={100} />
        </div>
      </div>
      <h2 className="upper-title">Contact</h2>
      <div className="contact-text">
        <p className="contact-info">ciprian.neamtu91@gmail.com</p>
        <p className="contact-info">0731 295 557</p>
        <p className="contact-info">Brasov</p>
        <p className="contact-info">Romania</p>
      </div>
    </div>
  );
};

export default Contact;
